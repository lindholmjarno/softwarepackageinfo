import fs from "fs";
export default (req, res) => {
  fs.readFile("sample/status", (err, data) => {
    let packages = [];
    // response data to string
    let dataString = data.toString();
    // split with \n\n to get each Package to own cell
    let dataArray = dataString.split("\n\n");
    dataArray.map(data => {
      let name = data.substring(
        data.indexOf("Package:") + 9,
        data.indexOf("\n")
      );
      let depends;
      if (data.indexOf("Depends:") === -1) {
        depends = "";
      } else {
        let dependIndex = data.indexOf("Depends:");
        depends = data.substring(
          dependIndex + 9,
          data.indexOf("\n", dependIndex)
        );
      }

      packages.push({ name: name, depends: depends });
    });

    res.status(200).json(packages);
  });
};
