import React, { useEffect, useState } from "react";
import axios from "axios";
const Index = () => {
  const [packages, setPackages] = useState([]);
  useEffect(() => {
    axios
      .get("/api/getSoftwarePackageInfo")
      .then(function(response) {
        // handle success
        setPackages(response.data);
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .then(function() {
        // always executed
      });
  }, []);
  const packageList = packages.map(pack => {
    return <li key={pack.name}>{pack.name}</li>;
  });

  return <div>{packageList}</div>;
};

export default Index;
